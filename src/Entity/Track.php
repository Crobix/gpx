<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 27/08/2018
 * Time: 15:23
 */

namespace App\Entity;


class Track
{
    private $points;

    private $totalDistance;

    private $totalTime;

    private $maxSpeed = 0;

    private $maxSpeed2s = 0;

    private $maxSpeed10s = 0;

    private $display;

    public function load(\SimpleXMLElement $datas)
    {
        $this->points = [];
        foreach ($datas->trk->trkseg->trkpt as $point) {
            $id = count($this->points);

            $this->points[] =
            $point = new Point($id, $point->ele, $point['lat'], $point['lon'], new \DateTime($point->time));
            $last = end($this->points);
            $prev = prev($this->points);
            if ($prev === false) {
                continue;
            }
            end($this->points)->setDistance(
                $this->totalDistance += $this->vincentyGreatCircleDistance(
                    $prev->getLatitude(),
                    $prev->getLongitude(),
                    $last->getLatitude(),
                    $last->getLongitude()
                )
            );
            end($this->points)->setSpeed(
                $speed =
                    round(
                        1.9438444924574 *
                        (($last->getDistance() - $prev->getDistance()) /
                            ($last->getTime()->format('U') - $prev->getTime()->format('U'))),
                        2
                    )
            );
            $this->display[] = ['x' => count($this->points), 'y' => $speed];

            $this->calculateMaxSpeed($speed);

            if (count($this->points) < 3) {
                continue;
            }

            $this->calcultateMaxSpeed2S();

            if (count($this->points) < 10) {
                continue;
            }

            $this->calculateMaxSpeed10S();
        }

        $this->totalTime = date_diff(end($this->points)->getTime(), $this->points[0]->getTime());
    }

    private function calculateMaxSpeed($speed)
    {
        if ($this->maxSpeed < $speed) {
            $this->maxSpeed = $speed;
        }
    }

    private function calcultateMaxSpeed2S()
    {
        if (
            $this->maxSpeed2s < $speed2s = (
                    end($this->points)->getSpeed() +
                    prev($this->points)->getSpeed() +
                    prev($this->points)->getSpeed()) / 3
        ) {
            $this->maxSpeed2s = $speed2s;
        }
    }

    private function calculateMaxSpeed10S()
    {
        if (
            $this->maxSpeed10s < $speed10s = (
                    end($this->points)->getSpeed() +
                    prev($this->points)->getSpeed() +
                    prev($this->points)->getSpeed() +
                    prev($this->points)->getSpeed() +
                    prev($this->points)->getSpeed() +
                    prev($this->points)->getSpeed() +
                    prev($this->points)->getSpeed() +
                    prev($this->points)->getSpeed() +
                    prev($this->points)->getSpeed() +
                    prev($this->points)->getSpeed()) / 10
        ) {
            $this->maxSpeed10s = $speed10s;
        }
    }

    private function vincentyGreatCircleDistance(
        $latitudeFrom,
        $longitudeFrom,
        $latitudeTo,
        $longitudeTo,
        $earthRadius = 6371000
    ) {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);
        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);
        $angle = atan2(sqrt($a), $b);
        return $angle * $earthRadius;
    }

    /**
     * @return mixed
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @return mixed
     */
    public function getTotalDistance()
    {
        return $this->totalDistance;
    }

    /**
     * @return mixed
     */
    public function getTotalTime()
    {
        return $this->totalTime;
    }

    /**
     * @return int
     */
    public function getMaxSpeed(): float
    {
        return $this->maxSpeed;
    }

    /**
     * @return int
     */
    public function getMaxSpeed2s(): float
    {
        return $this->maxSpeed2s;
    }

    /**
     * @return mixed
     */
    public function getDisplay()
    {
        $json = json_encode($this->display);
        $json = preg_replace('/"([^"]+)"\s*:\s*/', '$1:', $json);
        return $json;
    }

    /**
     * @param mixed $points
     */
    public function setPoints($points): void
    {
        $this->points = $points;
    }


    public function update()
    {
        $this->maxSpeed = 0;
        $this->maxSpeed2s = 0;
        $this->maxSpeed10s = 0;
        $this->display = [];

        foreach ($this->points as $point) {
            $this->display[] = ['x' => $point->getId(), 'y' => $point->getSpeed()];

            $this->calculateMaxSpeed($point->getSpeed());

            if ($point->getId() < 3) {
                continue;
            }

            if (isset($this->points[$point->getId() - 1]) && isset($this->points[$point->getId() - 2])) {
                $maxSpeed2s =
                    ($point->getSpeed() +
                        $this->points[$point->getId() - 1]->getSpeed() +
                        $this->points[$point->getId() - 2]->getSpeed()) / 3;
                if ($this->maxSpeed2s < $maxSpeed2s) {
                    $this->maxSpeed2s = $maxSpeed2s;
                }
            }

            if ($point->getId() < 10) {
                continue;
            }

            if (
                isset($this->points[$point->getId() - 1]) &&
                isset($this->points[$point->getId() - 2]) &&
                isset($this->points[$point->getId() - 3]) &&
                isset($this->points[$point->getId() - 4]) &&
                isset($this->points[$point->getId() - 5]) &&
                isset($this->points[$point->getId() - 6]) &&
                isset($this->points[$point->getId() - 7]) &&
                isset($this->points[$point->getId() - 8]) &&
                isset($this->points[$point->getId() - 9])
            ) {
                $maxSpeed10s = (
                        $point->getSpeed() +
                        $this->points[$point->getId() - 1]->getSpeed() +
                        $this->points[$point->getId() - 2]->getSpeed() +
                        $this->points[$point->getId() - 3]->getSpeed() +
                        $this->points[$point->getId() - 4]->getSpeed() +
                        $this->points[$point->getId() - 5]->getSpeed() +
                        $this->points[$point->getId() - 6]->getSpeed() +
                        $this->points[$point->getId() - 7]->getSpeed() +
                        $this->points[$point->getId() - 8]->getSpeed() +
                        $this->points[$point->getId() - 9]->getSpeed()) / 10;
                if ($this->maxSpeed10s < $maxSpeed10s) {
                    $this->maxSpeed10s = $maxSpeed10s;
                }
            }

        }
    }

    /**
     * @return int
     */
    public function getMaxSpeed10s(): int
    {
        return $this->maxSpeed10s;
    }

}
