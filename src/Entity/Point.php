<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 27/08/2018
 * Time: 14:32
 */

namespace App\Entity;


class Point
{
    private $id;

    private $altitude;

    private $latitude;

    private $longitude;

    private $time;

    private $distance;

    private $speed;

    public function __construct(int $id, string $altitude, string $latitude, string $longitude, \DateTime $time)
    {
        $this->id = $id;
        $this->altitude = $altitude;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->time = $time;
    }

    /**
     * @return string
     */
    public function getAltitude(): string
    {
        return $this->altitude;
    }

    /**
     * @param string $altitude
     */
    public function setAltitude(string $altitude): void
    {
        $this->altitude = $altitude;
    }

    /**
     * @return string
     */
    public function getLatitude(): string
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     */
    public function setLatitude(string $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return string
     */
    public function getLongitude(): string
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     */
    public function setLongitude(string $longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @return string
     */
    public function getTime(): \DateTime
    {
        return $this->time;
    }

    /**
     * @param string $time
     */
    public function setTime(\DateTime $time): void
    {
        $this->time = $time;
    }

    /**
     * @return mixed
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * @param mixed $distance
     */
    public function setDistance($distance): void
    {
        $this->distance = $distance;
    }

    /**
     * @return mixed
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @param mixed $speed
     */
    public function setSpeed($speed): void
    {
        $this->speed = $speed;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
