<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 28/08/2018
 * Time: 10:02
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class DeleteController extends AbstractController
{
    public function __invoke(Request $request)
    {
        $id = $request->get('id', null);
        if (is_null($id)) {
            return $this->redirectToRoute('liste');
        }

        $session = $request->getSession();
        $track = $session->get('track', null);

        if (is_null($track)) {
            return $this->redirectToRoute('index');
        }

        $datas = $track->getPoints();
        unset($datas[intval($id)]);
        $track->setPoints($datas);

        $track->update();

        $session->set('track', $track);

        return $this->redirectToRoute('liste');

    }
}
