<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 28/08/2018
 * Time: 09:37
 */

namespace App\Controller;


use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ListeController extends AbstractController
{
    private $paginator;

    public function __construct(PaginatorInterface $paginator)
    {
        $this->paginator = $paginator;
    }

    public function __invoke(Request $request)
    {
        $session = $request->getSession();

        $pagination = $this->paginator->paginate(
            $session->get('track', null)->getPoints(), /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render(
            'liste.html.twig',
            [
                'pagination' => $pagination,
            ]
        );
    }
}
