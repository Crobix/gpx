<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 28/08/2018
 * Time: 09:31
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class AnalyseController extends AbstractController
{
    public function __invoke(Request $request)
    {
        $session = $request->getSession();
        $time = $session->get('time', null);
        $track = $session->get('track', null);

        if (is_null($time) || is_null($track)) {
            $this->redirectToRoute('index');
        }

        return $this->render(
            'result.html.twig',
            [
                'time' => $time,
                'track' => $track,
            ]
        );
    }
}
