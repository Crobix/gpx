<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 27/08/2018
 * Time: 14:40
 */

namespace App\Controller;

use App\Entity\Track;
use App\Form\UploadFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelInterface;

class DefaultController extends AbstractController
{
    private $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    public function __invoke(Request $request)
    {
        $session = $request->getSession();
        $session->clear();
        $forms = $this->createForm(UploadFile::class);

        $forms->handleRequest($request);

        if ($forms->isSubmitted() && $forms->isValid()) {
            $forms->getData()['file']->move($this->kernel->getProjectDir() . '/../public/upload', 'latest.gpx');
            $track = new Track();
            $start = microtime(true);
            $track->load(simplexml_load_file($this->kernel->getProjectDir() . '/../public/upload/latest.gpx'));
            $time = round((microtime(true) - $start), 3);
            $session->set('time', $time);
            $session->set('track', $track);
            return $this->redirectToRoute('analyse');
        }

        return $this->render(
            'upload_file.html.twig',
            [
                'form' => $forms->createView(),
            ]

        );
    }
}
