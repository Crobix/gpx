@echo off

git checkout master
git pull
php composer.phar self-update
php composer.phar install -o --no-dev

pause
